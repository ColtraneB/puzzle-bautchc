//============================================================================
// Name        : map.h
// Author      : Coltrane Bautch
// Assignment  : CS 2040, Lab 6/7
// Description : Cell and its subclasses (empty, wumpus, pit,
//               river, arrow, plank)
//============================================================================

#include "cell.h"
#include "map.h"
#include "player.h"
#include <iostream>
#include <cstdlib>

using namespace std;

char EmptyCell::getChar() {
    return '.';
}

bool EmptyCell::enter(Player *player, Map* map) {
    return true;
}

void Cell::plank(Map* map, int x, int y) {
    cout << "Nothing happens." << endl;
}

char RiverCell::getChar() {
    return 'B';
}

void RiverCell::printSenses() {
    cout << "You hear rushing water nearby." << endl;
}

bool RiverCell::enter(Player *player, Map *map) {
    int newX;
    int newY;
    if (map->getVerticalRiver()) {
        newX = Map::wrapCoordinate(player->getX() + ((rand() % 2) * 2) - 1);
        newY = rand() % Map::SIZE;
        while (map->getCell(newX, newY)->getChar() != '.') {
            newX = Map::wrapCoordinate(player->getX() + ((rand() % 2) * 2) - 1);
            newY = rand() % Map::SIZE;
        }
    } else {
        newY = Map::wrapCoordinate(player->getY() + ((rand() % 2) * 2) - 1);
        newX = rand() % Map::SIZE;
        while (map->getCell(newX, newY)->getChar() != '.') {
            newY = Map::wrapCoordinate(player->getY() + ((rand() % 2) * 2) - 1);
            newX = rand() % map->SIZE;
        }
    }
    player->move(newX, newY);
    cout << "You fell into the river and flowed downstream" << endl;
    return true;
}

void RiverCell::plank(Map* map, int x, int y) {
    cout << "You created a bridge" << endl;
    map->clearCell(x, y);
}

char WumpusCell::getChar() {
    return '!';
}

void WumpusCell::printSenses() {
    cout << "You smell a Wumpus nearby." << endl;
}

bool WumpusCell::enter(Player *player, Map *map) {
    cout << "You were eaten by the Wumpus" << endl
         << "You lose" << endl;
    return false;
}

bool WumpusCell::shoot(Player *player) {
    cout << "You shot an arrow" << endl
         << "You hit the wumpus" << endl
         << "You win" << endl;
    return false;
}

void WumpusCell::plank(Map *map, int x, int y) {
    cout << "You hit something hard and Wumpus-shaped." << endl;
}

char PitCell::getChar() {
    return '@';
}

void PitCell::printSenses() {
    cout << "You feel a breeze" << endl;
}

bool PitCell::enter(Player *player, Map *map) {
    cout << "You fell into a pit" << endl
         << "You lose" << endl;
    return false;
}

void PitCell::plank(Map *map, int x, int y) {
    cout << "You hear the plank clatter down below" << endl;
}

char ArrowCell::getChar() {
    return '-';
}

bool ArrowCell::enter(Player *player, Map *map) {
    player->pickUpArrow();
    cout << "You found an arrow" << endl
         << "You now have " << player->getArrows() << " arrow"
         << (player->getArrows() != 1 ? "s" : "") << endl;
    return true;
}

void ArrowCell::plank(Map *map, int x, int y) {
    cout << "You hear something crushed underneath the plank" << endl;
    map->clearCell(x, y);
}

char PlankCell::getChar() {
    return '?';
}

bool PlankCell::enter(Player *player, Map *map) {
    player->pickUpPlank();
    cout << "You found a plank" << endl
         << "You now have " << player->getPlanks() << " plank"
         << (player->getPlanks() != 1 ? "s" : "") << endl;
    return true;
}

void Cell::printSenses() {}

bool Cell::shoot(Player* player) {
    cout << "You shot an arrow" << endl
         << "You missed" << endl
         << "You have " << player->getArrows() << " arrow"
         << (player->getArrows() != 1 ? "s" : "") << " left" << endl;
    return true;
}
