//============================================================================
// Name        : map.h
// Author      : Coltrane Bautch
// Assignment  : CS 2040, Lab 6/7
// Description : Contains the Map class, which represents the game map
//============================================================================

#include "map.h"
#include "cell.h"
#include "player.h"
#include <cstdlib>
#include <iostream>

using namespace std;

Map::Map() {
    player = new Player();

    for (int x = 0; x < SIZE; x++) {
        for (int y = 0; y < SIZE; y++) {
            cells[x][y] = nullptr;
        }
    }

    populateMap();
}

void Map::populateMap() {
    placeRiver();
    placeEntity(new WumpusCell());
    for (int i = 0; i < NUM_PITS; ++i) {
        placeEntity(new PitCell());
    }
    for (int i = 0; i < NUM_ARROWS; ++i) {
        placeEntity(new ArrowCell());
    }
    for (int i = 0; i < NUM_PLANKS; ++i) {
        placeEntity(new PlankCell());
    }
    placePlayer();
    fillEmptyCells();
}

void Map::fillEmptyCells() {
    for (int x = 0; x < SIZE; ++x) {
        for (int y = 0; y < SIZE; ++y) {
            if (cells[x][y] == nullptr) {
                cells[x][y] = new EmptyCell();
            }
        }
    }
}

void Map::placeRiver() {
    verticalRiver = (rand() % 2) == 0;
    if(verticalRiver) {
        int riverX = rand() % SIZE;
        for (int y = 0; y < SIZE; ++y) {
            cells[riverX][y] = new RiverCell();
        }
    } else {
        int riverY = rand() % SIZE;
        for (int x = 0; x < SIZE; ++x) {
            cells[x][riverY] = new RiverCell();
        }
    }
}

void Map::printMap() {
    for (int y = 0; y < SIZE; ++y) {
        for (int x = 0; x < SIZE; ++x) {
            if (x == player->getX() && y == player->getY()) {
                cout << '+';
            } else {
                cout << cells[x][y]->getChar();
            }
            cout << ' ';
        }
        cout << endl;
    }
}

Map::~Map() {
    for (int x = 0; x < SIZE; x++) {
        for (int y = 0; y < SIZE; y++) {
            delete cells[x][y];
        }
    }

    delete player;
}

void Map::printSenses() {
    for (int x = player->getX() - 1; x <= player->getX() + 1; ++x) {
        for (int y = player->getY() - 1; y <= player->getY() + 1; ++y) {
            cells[wrapCoordinate(x)][wrapCoordinate(y)]->printSenses();
        }
    }
}

void Map::placePlayer() {
    int newX = rand() % SIZE;
    int newY = rand() % SIZE;
    while (cells[newX][newY] != nullptr) {
        newX = rand() % SIZE;
        newY = rand() % SIZE;
    }
    player->move(newX, newY);
}

void Map::placeEntity(Cell *element) {
    int newX = rand() % SIZE;
    int newY = rand() % SIZE;
    while (cells[newX][newY] != nullptr) {
        newX = rand() % SIZE;
        newY = rand() % SIZE;
    }
    cells[newX][newY] = element;
}

bool Map::getVerticalRiver() const {
    return verticalRiver;
}

Cell *Map::getCell(int x, int y) {
    return cells[x][y];
}

bool Map::movePlayer(int xOffset, int yOffset) {
    // converts arrow and plank cells to empty cells after left
    if (cells[player->getX()][player->getY()]->getChar() != '.') {
        clearCell(player->getX(), player->getY());
    }
    player->move(wrapCoordinate(player->getX() + xOffset),
                 wrapCoordinate(player->getY() + yOffset));
    return cells[player->getX()][player->getY()]->enter(player, this);
}

int Map::wrapCoordinate(int coord) {
    if (coord < 0) {
        coord = SIZE - 1;
    } else if (coord >= SIZE) {
        coord = 0;
    }
    return coord;
}

bool Map::shootArrow(int xOffset, int yOffset) {
    bool running = true;
    if (player->useArrow()) {
        cout << "You shoot the arrow" << endl;
        running = cells[wrapCoordinate(player->getX() + xOffset)]
        [wrapCoordinate(player->getY() + yOffset)]->shoot(player);
    } else {
        cout << "You don't have any arrows" << endl;
    }
    return running;
}

bool Map::usePlank(int xOffset, int yOffset) {
    if (player->usePlank()) {
        cout << "You use the plank" << endl;
        cells[wrapCoordinate(player->getX() + xOffset)]
             [wrapCoordinate(player->getY() + yOffset)]
             ->plank(this, wrapCoordinate(player->getX() + xOffset),
                     wrapCoordinate(player->getY() + yOffset));
        cout << "You now have " << player->getPlanks() << " plank"
             << (player->getPlanks() != 1 ? "s" : "") << " left" << endl;
    } else {
        cout << "You don't have any planks" << endl;
    }
    return true;
}

void Map::clearCell(int x, int y) {
    delete cells[x][y];
    cells[x][y] = new EmptyCell();
}

void Map::debugMode() {
    // reset player
    delete player;
    player = new Player();
    for (int x = 0; x < SIZE; x++) {
        for (int y = 0; y < SIZE; y++) {
            delete cells[x][y];
            cells[x][y] = nullptr;
        }
    }
    // import custum map
    cells[0][0] = new RiverCell();
    cells[0][1] = new RiverCell();
    cells[0][2] = new RiverCell();
    cells[0][3] = new RiverCell();
    cells[0][4] = new RiverCell();
    cells[0][5] = new RiverCell();
    cells[4][0] = new ArrowCell();
    cells[2][1] = new PlankCell();
    cells[3][1] = new PlankCell();
    cells[4][1] = new ArrowCell();
    cells[5][1] = new WumpusCell();
    cells[2][2] = new PlankCell();
    cells[4][2] = new PlankCell();
    cells[5][2] = new PlankCell();
    cells[2][3] = new PlankCell();
    cells[3][3] = new PlankCell();
    cells[4][3] = new PlankCell();
    cells[5][3] = new PitCell();
    fillEmptyCells();
    player->move(3, 2);
}

