//
// Created by bautchc on 5/21/2023.
//
#include "player.h"

void Player::move(int x, int y) {
    this->x = x;
    this->y = y;
}

int Player::getX() const {
    return x;
}

int Player::getY() const {
    return y;
}

void Player::pickUpArrow() {
    arrows++;
}

void Player::pickUpPlank() {
    planks++;
}

int Player::getArrows() const {
    return arrows;
}

int Player::getPlanks() const {
    return planks;
}

bool Player::useArrow() {
    bool hasArrows = arrows > 0;
    if (hasArrows) {
        arrows--;
    }
    return hasArrows;
}

bool Player::usePlank() {
    bool hasPlanks = planks > 0;
    if (hasPlanks) {
        planks--;
    }
    return hasPlanks;
}

