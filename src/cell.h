//============================================================================
// Name        : map.h
// Author      : Coltrane Bautch
// Assignment  : CS 2040, Lab 6/7
// Description : Header file for cell and its subclasses (empty, wumpus, pit,
//               river, arrow, plank)
//============================================================================

#ifndef CELL_H_
#define CELL_H_

class Map;
class Player;

// abstract class for a cell on the map
class Cell {
public:
    // returns the char that visually represnts the cell (also used to identify type of subclass)
    virtual char getChar() = 0;
    // handles a player entering the cell
    // return value is whether the gameplay loop continues, so returns false on win or loss
    virtual bool enter(Player* player, Map* map) = 0;
    // prints message when player is adjacent
    virtual void printSenses();
    // handles player shooting at cell
    // return value is whether the gameplay loop continues, so returns false if wumpus is killed
    virtual bool shoot(Player* player);
    // handles player placing plank on cell
    virtual void plank(Map* map, int x, int y);
    // destructor
    virtual ~Cell() = default;
};

// empty cell
class EmptyCell : public Cell {
public:
    // returns '.'
    char getChar() override;
    // nothing happens
    bool enter(Player* player, Map* map) override;
};

// river cell
class RiverCell : public Cell {
public:
    // returns 'B'
    char getChar() override;
    // prints that player hears rushing water
    void printSenses() override;
    // moves the player to a random cell adjacent to the river
    bool enter(Player* player, Map* map) override;
    // turns the cell into an empty cell
    void plank(Map* map, int x, int y) override;
};

// wumpus cell
class WumpusCell : public Cell {
public:
    // returns '!'
    char getChar() override;
    // prints that player smells a wumpus
    void printSenses() override;
    // player is eaten by the wumpus (returns false)
    bool enter(Player* player, Map* map) override;
    // player shoots the wumpus (returns false)
    bool shoot(Player* player) override;
    // player can tell that wumpus is there
    void plank(Map* map, int x, int y) override;
};

// pit cell
class PitCell : public Cell {
public:
    // returns '@'
    char getChar() override;
    // prints that player feels a breeze
    void printSenses() override;
    // player falls into the pit (returns false)
    bool enter(Player* player, Map* map) override;
    // player loses the plank in the pit
    void plank(Map* map, int x, int y) override;
};

// arrow cell
class ArrowCell : public Cell {
public:
    // returns '-'
    char getChar() override;
    // player picks up an arrow
    bool enter(Player* player, Map* map) override;
    // arrow is crushed
    void plank(Map* map, int x, int y) override;
};

// plank cell
class PlankCell : public Cell {
public:
    // returns '?'
    char getChar() override;
    // player picks up a plank
    bool enter(Player* player, Map* map) override;
};

#endif //CELL_H_
