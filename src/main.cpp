//============================================================================
// Name        : main.cpp
// Author      : Coltrane Bautch
// Assignment  : CS 2040, Lab 6/7
// Description : Main for Wumpus Hunt game
//============================================================================

#include "map.h"
#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

// Prints the help info
void printHelp();
// Shoots an arrow in the given direction
bool shootArrow(const string& command, Map* map);
// Places a plank in the given direction
bool placePlank(const string& command, Map* map);

int main(int argc, char *argv[]) {
    srand(time(nullptr));
    Map* map = new Map();
    bool running = true;
    bool debug = false;
    string command;
    cout << "Welcome to Wumpus Hunt!" << endl
         << "Press [h] for rules and controls or begin playing" << endl;
    while (running) {
        map->printSenses();
        if (debug) {
            map->printMap();
        }
        cin >> command;
        for (char& c : command) {
            c = tolower(c);
        }
        switch (command[0]) {
            case 'h':
                printHelp();
                break;
            case 'd':
                cout << "Entering debug mode" << endl;
                debug = true;
                map->debugMode();
                break;
            case 'm':
                map->printMap();
                break;
            case 'a':
                running = shootArrow(command, map);
                break;
            case 'p':
                running = placePlank(command, map);
                break;
            case 'n':
                cout << "You moved north" << endl;
                running = map->movePlayer(0, -1);
                break;
            case 'e':
                cout << "You moved east" << endl;
                running = map->movePlayer(1, 0);
                break;
            case 's':
                cout << "You moved south" << endl;
                running = map->movePlayer(0, 1);
                break;
            case 'w':
                cout << "You moved west" << endl;
                running = map->movePlayer(-1, 0);
                break;
            default:
                cout << "Invalid command" << endl;
                break;
        }
    }

    delete map;
}

bool shootArrow(const string& command, Map* map) {
    bool running = true;
    if (command.length() > 1) {
        switch (command[1]) {
            case 'n':
                running = map->shootArrow(0, -1);
                break;
            case 'e':
                running = map->shootArrow(1, 0);
                break;
            case 's':
                running = map->shootArrow(0, 1);
                break;
            case 'w':
                running = map->shootArrow(-1, 0);
                break;
            default:
                cout << "Invalid direction" << endl;
                break;
        }
    } else {
        cout << "You need to include a direction when you shoot an arrow" << endl;
    }
    return running;
}

void printHelp() {
    cout << "This version of Wumpus Hunt takes place on a 6 by 6 grid with wrapping" << endl
         << "The goal is to deduce the location of the Wumpus and kill it using an arrow" << endl
         << "Although you cannot see the map, you can use your sense to know when certain things "
         << " are nearby" << endl
         << "Wumpuses give off a distinctive smell" << endl
         << "Pits can be felt by a breeze" << endl
         << "Rivers can be heard by the sound of running water" << endl
         << "Arrows and planks can only be detected when you enter a tile with them" << endl
         << "You can move around and perform actions with the following controls:" << endl
         << "h: printMap rules" << endl
         << "d: turn on debug mode" << endl
         << "m: display map" << endl
         << "n: move north" << endl
         << "e: move east" << endl
         << "s: move south" << endl
         << "w: move west" << endl
         << "ax: shoot arrow (where x is replaced with the direction to be shot)" << endl
         << "px: place plank (where x is replaced with the direction to place the plank)" << endl
         << "If you fall into a pit or a Wumpus, you will lose the game" << endl
         << "If you fall into the river, you will be transported to another tile on either side of"
         << "the river" << endl
         << "If you enter a square with an arrow or plank, it will be added to your inventory"
         << endl
         << "You begin with 5 arrows, and you can shoot them in any of the four directions" << endl
         << "If you hit an adjacent Wumpus with your arrow, you will win the game" << endl
         << "If you miss the Wumpus, you will lose that arrow" << endl
         << "You do not start with any planks, but you can pick them up." << endl
         << "Placing a plank consumes it, but different things happen depending on what's in the "
         << "square where you place it" << endl
         << "When placing a plank on a Wumpus, you will be able to tell that a Wumpus is there"
         << endl
         << "When placing a plank on a pit, you will hear the plank fall in" << endl
         << "When placing a plank on a river, you will create a bridge over the river" << endl
         << "When placing a plank on an arrow, you will crush the arrow" << endl
         << "When placing a plank on an empty square, you will lose the plank" << endl
         << "When displaying the map, the following key is used:" << endl
         << "+: player" << endl
         << ".: empty" << endl
         << "!: Wumpus" << endl
         << "@: Pit" << endl
         << "B: River" << endl
         << "-: Arrow" << endl
         << "?: Plank" << endl;
}

bool placePlank(const string& command, Map* map) {
    bool running = true;
    if (command.length() > 1) {
        switch (command[1]) {
            case 'n':
                running = map->usePlank(0, -1);
                break;
            case 'e':
                running = map->usePlank(1, 0);
                break;
            case 's':
                running = map->usePlank(0, 1);
                break;
            case 'w':
                running = map->usePlank(-1, 0);
                break;
            default:
                cout << "Invalid direction" << endl;
                break;
        }
    } else {
        cout << "You need to include a direction when you use a plank" << endl;
    }
    return running;
}