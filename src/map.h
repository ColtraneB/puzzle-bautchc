//============================================================================
// Name        : map.h
// Author      : Coltrane Bautch
// Assignment  : CS 2040, Lab 6/7
// Description : Header file for game map
//============================================================================

#ifndef MAP_H_
#define MAP_H_

class Player;
class Cell;

// controls the map and interactions with it
class Map {
public:
    static constexpr int SIZE = 6;
    static constexpr int NUM_PITS = 2;
    static constexpr int NUM_ARROWS = 3;
    static constexpr int NUM_PLANKS = 2;
    // initialize map with random entity placement
    Map();
    // deconstructor
    ~Map();
    // print map to cout
    void printMap();
    // print messages from cells adjacent to player
    void printSenses();
    // get whether the river is vertical
    bool getVerticalRiver() const;
    // get cell at location
    Cell* getCell(int x, int y);
    // move player
    bool movePlayer(int xOffset, int yOffset);
    // shoot arrow
    bool shootArrow(int xOffset, int yOffset);
    // place plank
    bool usePlank(int xOffset, int yOffset);
    // sets the cell at location to empty
    void clearCell(int x, int y);
    // corrects out of bounds coordinates
    static int wrapCoordinate(int coord);
    // turns on debug mode
    void debugMode();
private:
    Cell* cells[SIZE][SIZE];
    bool verticalRiver;
    Player* player;
    void populateMap();
    void fillEmptyCells();
    void placeRiver();
    void placePlayer();
    void placeEntity(Cell* element);
};

#endif /* MAP_H_ */