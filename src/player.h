//============================================================================
// Name        : map.h
// Author      : Coltrane Bautch
// Assignment  : CS 2040, Lab 6/7
// Description : Header file for player
//============================================================================

#ifndef PLAYER_H_
#define PLAYER_H_

// handles information related to the player
class Player {
public:
    static constexpr int START_ARROWS = 5;
    // move player to location
    void move(int x, int y);
    // get player's x coordinate
    int getX() const;
    // get player's y coordinate
    int getY() const;
    // pick up arrow
    void pickUpArrow();
    // pick up plank
    void pickUpPlank();
    // get number of arrows in inventory
    int getArrows() const;
    // get number of planks in inventory
    int getPlanks() const;
    // remove an arrow from inventory
    bool useArrow();
    // remove a plank from inventory
    bool usePlank();
private:
    int x;
    int y;
    int arrows = START_ARROWS;
    int planks = 0;
};

#endif //PLAYER_H_
