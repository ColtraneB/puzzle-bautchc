# Wumpus Hunt

By Coltrane Bautch

## Steps for building

This program was built using g++ 6.3.0. To build, run the following command from the src directory:

```bash
g++ -o wumpus_hunt cell.cpp cell.h main.cpp map.cpp map.h player.cpp player.h
```

## Help & Debug Mode

At any time during the running of the program, you can type \[d] to enter debug mode or \[h] to get the information and list of commands.

